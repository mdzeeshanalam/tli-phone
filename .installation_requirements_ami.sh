

# setting up the server requirements
### php version setting up

sudo apt purge php7.4*
sudo apt purge php7.3*


sudo add-apt-repository ppa:ondrej/php # Press enter when prompted.
sudo apt-get update
sudo apt install php8.0-common php8.0-cli -y

sudo apt install php8.0-{bz2,curl,intl,mysql,readline,xml}
sudo apt install php8.0-fpm
sudo apt install libapache2-mod-php8.0
sudo apt install php8.0-bcmath php8.0-ctype php8.0-fileinfo php8.0-mbstring php8.0-opcache php8.0-pdo php8.0-tokenizer php8.0-xml php8.0-curl php8.0-zip php8.0-gd

#Installation Steps
#Step 1
#Unzip the file you downloaded, It inclueds one zip file. You only need to upload the file "zip" file to your server and unzip it.

#Step 2
#Create an empty database in your MySQL server, Keep the credentials inculding:
#1. Host name or host ip-address
#2. Database name
#3. Database Username
#4. Password for database username

#Step 3
#Generally, If you are using cPanel, You may skip this step.
#But if you setup the server by yourself, You may check if the following folders are writable:
#1. /upload/, Always require 777 or 755, Apply to all files and subdirectories
chmod -R 777 /var/www/html/phone.christianfilipina.com/upload
#2. /backup/, Always require 777 or 755, Apply to directory only
chmod -R 777 /var/www/html/phone.christianfilipina.com/backup
#3. /application/config/, Only require 777 or 755 while installing the script, Apply to directory only
#You may navigate and set the proper permission using this command: chmod -R 777 folderName
chmod -R 777 /var/www/html/phone.christianfilipina.com/config

chown -R root:root $WEB_DIR/bootstrap
chown -R root:root $WEB_DIR/storage

chmod -R 644 /var/www/html/phone.christianfilipina.com/public
chmod -R 777 /var/www/html/phone.christianfilipina.com/storage

sudo chmod -R 755 /var/www/html/phone.christianfilipina.com



sudo chgrp -R www-data ./
sudo chown -R www-data:www-data ./
find ./ -type d -exec chmod 755 -R {} \;
find ./ -type f -exec chmod 644 {} \;

#Step 4
#Open your URL, You will be redirected to the installation page, Fill in the necessary information and click continue at every step.
#If all are ready then you will finally see the success page.

# setup the mysql aurora 8.0.26 database for the tliphone database

apt install composer

WEB_DIR="/var/www/html/phone.christianfilipina.com"
cd $WEB_DIR

composer install --no-dev --no-progress --prefer-dist --no-interaction
#composer update


cd  $WEB_DIR && sudo php artisan cache:clear
cd  $WEB_DIR && sudo php artisan view:clear
cd  $WEB_DIR && sudo php artisan config:cache
cd  $WEB_DIR && sudo php artisan config:clear
php artisan vendor:publish --tag=laravel-assets --ansi --force


sudo systemctl restart apache2
sudo service php8.0-fpm restart

php artisan migrate
php artisan key:generate









#Cronjob
#You should setup the four following Cronjob:
#1. For calculate the balance deductoin, Every 1 second:
/usr/bin/php $WEB_DIR/artisan calculate:deduction
#2. For call duration, Every 1 second:
/usr/bin/php $WEB_DIR/artisan call:duration
#3. For campaign call, Every 1 second:
/usr/bin/php $WEB_DIR/artisan start:calling
#3. For Expiry subscription alert, Every 1 second:
/usr/bin/php $WEB_DIR/artisan send:expiry-alert-mail

#Note:: You can set your own desired schedule as long as you want.

#Signin Credential
#Here is the default signin credentials:
#URL: yourdomain/login
#email: As given when installtion
#Password: As given when installtion







#<VirtualHost _default_:443>
#
#    ServerName phone.christianfilipina.com
#    ServerAlias phone.christianfilipina.com
#
#    ErrorLog /dev/null
#    #TransferLog logs/ssl_access_log
#    #LogLevel ssl:trace8
#
#    SSLEngine on
#    SSLProtocol             all
#    SSLCipherSuite          ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
#    SSLHonorCipherOrder     off
#    SSLSessionTickets       off
#
##    SSLOptions +StrictRequire
#
#    #SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
#    #<Files ~ "\.(cgi|shtml|phtml|php3?)$">
#        #SSLOptions +StdEnvVars
#    #</Files>
#
#    DocumentRoot    /var/www/html/phone.christianfilipina.com/public
#
#    <directory "/var/www/html/phone.christianfilipina.com">
#        options followsymlinks
#            allowoverride all
#            Require all granted
#    </directory>
#
#    # Include   /etc/letsencrypt/options-ssl-apache.conf
#
#    SSLCertificateFile /etc/letsencrypt/live/infra.christianfilipina.com/fullchain.pem
#    SSLCertificateKeyFile /etc/letsencrypt/live/infra.christianfilipina.com/privkey.pem
#</VirtualHost>
#
# <VirtualHost *:80>
#     ServerName phone.christianfilipina.com
#     ServerAlias phone.christianfilipina.com
#
#    ErrorLog /dev/null
#
#
#     DocumentRoot    /var/www/html/phone.christianfilipina.com/public
#
#     <directory "/var/www/html/phone.christianfilipina.com">
#         options followsymlinks
#             allowoverride all
#             Require all granted
#     </directory>
##     Redirect permanent / https://phone.christianfilipina.com/
##     RewriteEngine on
##     RewriteCond %{SERVER_NAME} =phone.christianfilipina.com [OR]
##    RewriteCond %{SERVER_NAME} =phone.christianfilipina.com
##     RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
# </VirtualHost>