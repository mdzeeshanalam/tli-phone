<?php

use App\Http\Controllers\IvrController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth', 'otp.verified', 'kyc.verified'], 'prefix' => 'dashboard'], function () {
	Route::get('/ivr', [IvrController::class, 'index'])->name('dashboard.ivr.index'); // Route name: dashboard.ivr.index
});

// version 3.0.0
Route::post('/process-response', [IvrController::class, 'processResponse'])->name('dialer.processResponse');