<?php

use App\Http\Controllers\DialerController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth', 'otp.verified', 'kyc.verified'], 'prefix' => 'dashboard'], function () {
	Route::get('/dialer', [DialerController::class, 'index'])->name('dialer.index');

	/**
	 * Call Duration
	 */
	Route::any('/dialer/call-duration/store', [DialerController::class, 'store'])->name('dialer.call-duration.store');
	Route::any('/dialer/check/country-code/exists-in-package', [DialerController::class, 'country_code_exists_in_package'])->name('dialer.country.code.exists.in.package');

	Route::get('/inbound-call/{my_number}', [DialerController::class, 'dialerpad'])->middleware('incoming.number.checker')->name('dialerpad');
	Route::post('/create-call-history', [DialerController::class, 'createCallHistory'])->name('create.call.hostory');
	Route::get('/dialer/token', [DialerController::class, 'dialer_token'])->name('dialer.token');
});

Route::any('/handle_call', [DialerController::class, 'handle_call'])->name('dialer.handle_call');
