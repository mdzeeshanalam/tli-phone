<?php

if (env('DEVELOPMENT_MODE') == 'YES') { // If development mode is enabled
	Auth::routes(); // Auth > Routes
} else {
	Auth::routes(['register' => false]); // Auth > Routes
}

Route::get('/test', function () {
	return get_user_identity(auth()->id());
}); // Home > Index
