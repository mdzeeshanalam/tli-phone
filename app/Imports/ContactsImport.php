<?php

namespace App\Imports;

use App\Models\Contact;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContactsImport implements ToModel, WithHeadingRow
{
	/**
	 * @param array $row
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row)
	{
		return new Contact([
			'user_id' => Auth::id(),
			'name' => $row['name'],
			'phone' => $row['phone'],
			'country' => $row['country'],
			'gender' => $row['gender'],
			'dob' => $row['dob'],
			'profession' => $row['profession'],
		]);
	}
}
