<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactsExport implements FromCollection, WithHeadings
{
	/**
	 * @return \Illuminate\Support\Collection
	 */
	public function collection()
	{
		return Contact::select('user_id', 'name', 'phone', 'country', 'gender', 'dob', 'profession')->get();
	}

	/**
	 * Write code on Method
	 *
	 * @return response()
	 */
	public function headings(): array
	{
		return ['user_id', 'name', 'phone', 'country', 'gender', 'dob', 'profession'];
	}
}
