<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OauthPersonalAccessClients extends Model
{
	use HasFactory;

	/**
	 * Agent
	 */
	public function scopeHasAgent($query)
	{
		if (Auth::user()->role == 'agent') {
			return $query->where('user_id', agent_owner_id());
		}
		return $query->where('user_id', Auth::user()->id);
	}
}
