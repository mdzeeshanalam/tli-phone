<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
	use HasFactory;

	protected $guarded = ['id'];

	public function subscription()
	{
		return $this->hasOne(Subscription::class, 'id', 'subscription_id');
	}
}
