<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
	use HasFactory;

	/**
	 * > This function returns all the campaigns that belong to this provider
	 *
	 * @return A collection of Campaigns
	 */
	public function campaigns()
	{
		return $this->hasMany(Campaign::class, 'provider_id', 'id');
	}

	/**
	 * It returns the campaign schedules that belong to the provider.
	 *
	 * @return The campaign_schedules() method returns a collection of CampaignSchedule objects.
	 */
	public function campaign_schedules()
	{
		return $this->hasMany(CampaignSchedule::class, 'provider_id', 'id');
	}

	/**
	 * If the user is an agent, return the query where the user_id is the agent_owner_id, otherwise
	 * return the query where the user_id is the current user's id
	 *
	 * @param query The query builder instance.
	 *
	 * @return A query builder object.
	 */
	public function scopeHasAgent($query)
	{
		if (Auth::user()->role == 'agent') {
			return $query->where('user_id', agent_owner_id());
		}
		return $query->where('user_id', Auth::user()->id);
	}

	/**
	 * This function returns a relationship between the current model and the User model
	 *
	 * @return A user object
	 */
	public function user()
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	// ENDS
}
