<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampaignSmsStatusLog extends Model
{
	use HasFactory;

	protected $guarded = ['id'];
}
