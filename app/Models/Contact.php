<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	use HasFactory;

	protected $guarded = [];

	/**
	 * Group Contacts
	 */
	public function group_contacts()
	{
		return $this->hasMany(GroupContact::class, 'contact_id', 'id');
	}

	/**
	 * Campaign Voice
	 */
	public function campaign_voice()
	{
		return $this->hasMany(CampaignVoice::class, 'contact_id', 'id');
	}

	/**
	 * CampaignVoiceStatusLog
	 */
	public function campaign_voice_status_log()
	{
		return $this->hasMany(CampaignVoiceStatusLog::class, 'contact_id', 'id');
	}

	/**
	 * Agent
	 */
	public function scopeHasAgent($query)
	{
		if (Auth::user()->role == 'agent') {
			return $query->where('user_id', agent_owner_id());
		}
		return $query->where('user_id', Auth::user()->id);
	}
}
