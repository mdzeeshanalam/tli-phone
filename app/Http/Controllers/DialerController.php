<?php

namespace App\Http\Controllers;

use App\Models\CallHistory;
use App\Models\Identity;
use App\Models\LiveCallDuration;
use App\Models\PackageSupportedCountry;
use App\Models\Provider;
use App\Models\TwilioCallCost;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Twilio\Jwt\ClientToken;
use Twilio\TwiML\VoiceResponse;

class DialerController extends Controller
{
	public function index()
	{
		generate_user_slug(auth()->id());
		return view('backend.dialer.index'); // backend.dialer.index
	}

	/**
	 * STORE
	 */
	public function store(Request $request)
	{

		if (Auth::user()->role == 'agent') { // agent
			if (check_balance(agent_owner_id()) == false) { // check_balance
				// return an error message
				return response()->json([
					'status' => 'error',
					'message' => 'You have insufficient balance to make this call.',
				]);
			}
		} else {
			if (check_balance(Auth::id()) == false) { // check_balance
				// return an error message
				return response()->json([
					'status' => 'error',
					'message' => 'You have insufficient balance to make this call.',
				]);
			}
		}

		if (userRoleByAuthId() != 'admin') { // Auth::user()->role
			$request->validate([ // validate
				'dialer_session_uuid' => 'required',
			], [
				'dialer_session_uuid.required' => 'Dial Session ID is required',
			]);

			// check the $request->phone has + sign or not
			if (substr($request->phone, 0, 1) != '+') {
				$request->phone = '+' . $request->phone;
			}

			// check country code exists in package
			$phone = $request->phone;
			$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
			try {
				$NumberProto = $phoneUtil->parse($phone, null);

				// Check country code exists in TwilioCallCost
				$check_TwilioCall_Cost = TwilioCallCost::where('code', $NumberProto->getCountryCode())
					->first();

				if (!$check_TwilioCall_Cost) { // if !$check_TwilioCall_Cost
					return response()->json([
						'status' => 'error',
						'message' => 'Unsupported number.',
					]);
				}

				// check twilio_call_costs_id in package supported countries
				$check_package_supported_countries = PackageSupportedCountry::where('twilio_call_costs_id', $check_TwilioCall_Cost->id)
					->where('package_id', activePackage()) // activePackage()
					->first(); // get package_id

				if (!$check_package_supported_countries) { // if !$check_package_supported_countries


					$check_sessoin_id = LiveCallDuration::where('dialer_session_uuid', $request->dialer_session_uuid)
						->first(); // get dialer_session_uuid

					if ($check_sessoin_id) { // if $check_sessoin_id
						$check_sessoin_id->end_at = Carbon::now();
						$check_sessoin_id->duration = $check_sessoin_id->end_at->diffInSeconds($check_sessoin_id->start_at);
						$check_sessoin_id->total_deduction = $check_sessoin_id->duration * $check_sessoin_id->app_deduction; // per sec cost * call seconds
						$check_sessoin_id->save();

						live_call_deduct_credit($check_sessoin_id->user_id, $check_sessoin_id->dialer_session_uuid); // deduct credit
					} else { // else $check_sessoin_id
						$live_call_duration = new LiveCallDuration;
						$live_call_duration->user_id = Auth::id();
						$live_call_duration->phone = $request->phone;
						$live_call_duration->dialer_session_uuid = $request->dialer_session_uuid;
						$live_call_duration->start_at = Carbon::now();
						// $live_call_duration->app_deduction = call_cost_per_second(get_country_code_from_number($phone)); // per seconds
						$live_call_duration->app_deduction = call_cost_per_second('880'); // per seconds
						$live_call_duration->save();
					}

					// return an success message
					return response()->json([
						'status' => 'success',
						'message' => 'Call duration has been saved.',
					]);


					return response()->json([
						'status' => 'success',
						'message' => 'This phone number is not eligble in your package. This call will be charged extra ' . $check_TwilioCall_Cost->tliphone_cost . '/min.' . '<br> To cancel this call, make the call end. Thank you.',
					]);
				}

			} catch (\libphonenumber\NumberParseException $e) { // catch NumberParseException
				// return an error message
			}

			// check country code exists in package::ENDS
			$check_sessoin_id = LiveCallDuration::where('dialer_session_uuid', $request->dialer_session_uuid)
				->first(); // get dialer_session_uuid

			if ($check_sessoin_id) { // if $check_sessoin_id
				$check_sessoin_id->end_at = Carbon::now();
				$check_sessoin_id->duration = $check_sessoin_id->end_at->diffInSeconds($check_sessoin_id->start_at);
				$check_sessoin_id->total_deduction = $check_sessoin_id->duration * $check_sessoin_id->app_deduction;
				$check_sessoin_id->save();

				live_call_deduct_credit($check_sessoin_id->user_id, $check_sessoin_id->dialer_session_uuid); // deduct credit
			} else { // else $check_sessoin_id
				$live_call_duration = new LiveCallDuration;
				$live_call_duration->user_id = Auth::id();
				$live_call_duration->phone = $request->phone;
				$live_call_duration->dialer_session_uuid = $request->dialer_session_uuid;
				$live_call_duration->start_at = Carbon::now();
				// $live_call_duration->app_deduction = call_cost_per_second(get_country_code_from_number($phone));
				$live_call_duration->app_deduction = call_cost_per_second('880');
				$live_call_duration->save();
			}
			// return an success message
			return response()->json([
				'status' => 'success',
				'message' => 'Call duration has been saved.',
			]);
		} else {
			return response()->json([
				'status' => 'success',
				'message' => 'Call duration has been saved.',
			]);
		}
	}

	/**
	 * dialer.country.code.exists.in.package
	 */
	public function country_code_exists_in_package(Request $request)
	{

		// check the $request->phone has + sign or not
		if (substr($request->phone, 0, 1) != '+') {
			$request->phone = '+' . $request->phone;
		}

		$phone = $request->phone;
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		try {
			$NumberProto = $phoneUtil->parse($phone, null);

			// Check country code exists in TwilioCallCost
			$check_TwilioCall_Cost = TwilioCallCost::where('code', $NumberProto->getCountryCode())
				->first();

			/* Checking if the number is supported. */
			if (!$check_TwilioCall_Cost) { // if !$check_TwilioCall_Cost
				return response()->json([
					'status' => 'error',
					'message' => 'Unsupported number.',
				]);
			}

			if (userRoleByAuthId() != 'admin') { // Auth::user()->role
				// check twilio_call_costs_id in package supported countries
				$check_package_supported_countries = PackageSupportedCountry::where('twilio_call_costs_id', $check_TwilioCall_Cost->id)
					->where('package_id', activePackage())
					->first(); // get package_id
			} else {
				// check twilio_call_costs_id in package supported countries
				$check_package_supported_countries = PackageSupportedCountry::where('twilio_call_costs_id', $check_TwilioCall_Cost->id)
					->first(); // get package_id
			}


			if (!$check_package_supported_countries) { // if !$check_package_supported_countries
				return response()->json([
					'status' => 'error',
					'message' => 'This phone number is not eligible in your package. This call will be charged extra ' . $check_TwilioCall_Cost->tliphone_cost . '<br> To cancel this call, make the call end. Thank you.',
				]);
			}

		} catch (\libphonenumber\NumberParseException $e) { // catch NumberParseException
			return response()->json([
				'status' => 'error',
				'message' => 'warning: ' . $e,
			]);
		}

	}

	/**
	 * It generates a token for the Twilio Client SDK to use to connect to Twilio
	 */
	public function dialer_token()
	{
		// put your Twilio API credentials here

		$accountSid = user_provider_info(auth()->id())->account_sid;
		$authToken = user_provider_info(auth()->id())->auth_token;
		$appSid = user_provider_info(auth()->id())->capability_token;

		/* Generating a token for the client to use to connect to Twilio. */
		$capability = new ClientToken($accountSid, $authToken);
		$capability->allowClientOutgoing($appSid);
		$capability->allowClientIncoming(get_user_identity(auth()->id()));
		$token = $capability->generateToken(3600 * 12);
		$data['identity'] = get_user_identity(auth()->id());
		$data['token'] = $token;
		echo json_encode($data);
	}

	/* A Twilio function. It is used to handle the call. */
	public function handle_call(Request $request)
	{

		/* Taking the data from the form and storing it in the variable . */
		$data = $request->all();

		/* The above code is creating a new voice response object. */
		$response = new VoiceResponse();

		// $response->say('For quality assurance, This call my be recorded.', ['voice' => 'woman', 'language' => 'en']);

		if (isset($data['ApplicationSid'])) {

			/* Assigning the value of the key 'ApplicationSid' to the variable . */
			$applicationSid = $data['ApplicationSid'];

			/* Finding the provider with the capability token that matches the applicationSid. */
			$provider = Provider::where('capability_token', $applicationSid)->first();

			/* Getting the phone number of the provider. */
			$twilio_number = $provider->phone;

			$response->dial($data['To'],
				[
					'callerId' => $twilio_number,
					'record' => 'record-from-answer-dual',
					'action' => '',
					'timeout' => 20,
					'method' => 'POST'
				]
			);

		} else {

			$user_id = Provider::where('phone', $data['Called'])->first()->user_id;
			$identity = Identity::where('user_id', $user_id)->first()->identity;

			$dial = $response->dial('',
				[
					'record' => 'record-from-answer-dual',
					'action' => '',
					'timeout' => 20,
					'method' => 'POST'
				]
			);
			$dial->client($identity);

		}

		return response($response)->header('Content-Type', 'text/xml');

		// ------------------------- IVR -------------------------------

		// $response = new VoiceResponse();
		// $response->say('Welcome to my IVR system. Please wait while we connect you.');

		// // introduce a 5 second delay
		// $response->pause(['length' => 2]);

		// /* The above code is using the Twilio PHP library to create a gather verb, which prompts the
		// user to input a single digit using either speech or DTMF (dual-tone multi-frequency) tones.
		// The gather verb has a timeout of 5 seconds and will automatically detect speech or DTMF
		// input. Once the user inputs a digit, the response will be sent to the
		// '/twilio/process-response' endpoint for further processing. */
		// $gather = $response->gather([
		//     'numDigits' => 1,
		//     'input' => 'speech dtmf',
		//     'timeout' => 5,
		//     'speechTimeout' => 'auto',
		//     'digitTimeout' => 5,
		//     'action' => '/twilio/process-response'
		// ]);

		// /* The above code is using PHP to create a message that prompts the user to input a number.
		// Depending on the number inputted, the user will either receive an offer, be directed to book
		// a flight, be directed to explore further options, or be connected with an agent for
		// assistance. */
		// $gather->say('Hello! Press 1 to get the offer. Press 2 to fly now. Press 3 to go beyond. Press 0 to talk with an agent.');

		// // if the user doesn't press any key within 5 seconds, end the call
		// $response->say("Sorry, we didn't receive any input. Goodbye.");
		// $response->hangup();

		// /* The above code is a PHP code that is returning a response with a specified content type of
		// "text/xml". The  variable is likely holding some XML data that is being returned as
		// the response. */
		// return response($response)->header('Content-Type', 'text/xml');


	}

	/**
	 * This PHP function returns a view for a dialer pad with a given phone number.
	 *
	 * @param my_number The parameter `` is a variable that contains a phone number. It is
	 * being passed to the `dialerpad` function as an argument and then being compacted with the
	 * `dialerpad` view. The compacted variable can then be accessed in the view to display the
	 *
	 * @return a view called 'backend.dialer.dialerpad' with the variable  passed to it.
	 */
	public function dialerpad($my_number)
	{
		return view('backend.dialer.dialerpad', compact('my_number')); // backend.dialer.dialerpad
	}

	/**
	 * The function creates a new call history record with the provided request data and returns a JSON
	 * response.
	 *
	 * @param Request request  is an instance of the Request class which contains the data sent
	 * in the HTTP request. It is used to retrieve data from the request such as form data, query
	 * parameters, and request headers. In this case, the function is using the  object to
	 * retrieve data for creating a new CallHistory
	 *
	 * @return A JSON response with the newly created CallHistory object and a status code of 200.
	 */
	public function createCallHistory(Request $request)
	{

		/* The above code is querying the database for a CallHistory record that matches the specified
		conditions. The conditions include the authenticated user's ID, the user's identity ID, the
		user's phone number, the caller's phone number, and the caller's UUID session. If a matching
		record is found, it is returned as a CallHistory object. */
		$history = CallHistory::where([
			'user_id' => auth()->id(),
			'identity_id' => get_user_identity_id(auth()->id()),
			'my_number' => $request->my_number,
			'caller_uuid' => $request->get_caller_uuid_session,
		])->first();

		/* The above code is a conditional statement in PHP. It checks if the variable `` is
		truthy (i.e. not null, 0, false, or an empty string). If `` is truthy, then the code
		inside the curly braces (`{}`) will be executed. However, since the code inside the curly
		braces is commented out with ` */
		if ($history) {

			/* The above code is checking if the `pick_up_time` property of the `` object is
			null. If it is null, it sets the `pick_up_time` property to the current time using the
			`now()` function. */
			if ($history->pick_up_time == null) {
				$history->pick_up_time = now();
			}

			/* The above code is checking if the `hang_up_time` property of the `` object is
			null. If it is null, it sets the `hang_up_time` property to the current time using the
			`now()` function. */
			if ($history->hang_up_time == null) {
				$history->hang_up_time = now();
			}

			$history->record_file = $request->record_file ?? null;
			$history->status = $request->status;
			$history->save();
		} else {
			$history = CallHistory::create([
				'user_id' => auth()->id(),
				'identity_id' => get_user_identity_id(auth()->id()),
				'my_number' => $request->my_number,
				'caller_number' => $request->caller_number,
				'caller_uuid' => $request->get_caller_uuid_session,
				'pick_up_time' => now(),
				'hang_up_time' => $request->hang_up_time ?? null,
				'record_file' => $request->record_file ?? null,
				'status' => $request->status,
			]);
		}

		/* The above code is a PHP code that returns a JSON response with a variable named ``
		and a status code of 200. The content of the `` variable is not shown in the code
		snippet. */
		return response()->json($history, 200);

	}
	//ENDS
}
