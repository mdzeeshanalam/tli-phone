<?php

namespace App\Http\Controllers;

use App\Models\AgentJobBoard;
use Illuminate\Http\Request;

class AgentJobBoardController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Models\AgentJobBoard $agentJobBoard
	 * @return \Illuminate\Http\Response
	 */
	public function show(AgentJobBoard $agentJobBoard)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param \App\Models\AgentJobBoard $agentJobBoard
	 * @return \Illuminate\Http\Response
	 */
	public function edit(AgentJobBoard $agentJobBoard)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \App\Models\AgentJobBoard $agentJobBoard
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, AgentJobBoard $agentJobBoard)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param \App\Models\AgentJobBoard $agentJobBoard
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(AgentJobBoard $agentJobBoard)
	{
		//
	}
}
