<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Illuminate\Http\Request;
use Log;
use Twilio\TwiML\VoiceResponse;

class IvrController extends Controller
{

	/**
	 * Display a listing of the resource.
	 */
	public function index()
	{
		return view('backend.ivr.index');
	}

	/**
	 * The function processes a response based on user input and generates a TwiML response with a
	 * message and logs the response.
	 *
	 * @param Request request  is an object of the Request class, which is a representation of
	 * an HTTP request made to the server. It contains information about the request, such as the HTTP
	 * method, headers, and parameters. In this case, it is used to retrieve the input 'Digits'
	 * parameter sent from the user's
	 *
	 * @return a TwiML response in XML format with a message based on the user's input (selected digit)
	 * and a hangup command. The response header is set to 'Content-Type' of 'text/xml'.
	 */
	public function processResponse(Request $request)
	{
		$twiml = new VoiceResponse();

		/* The `switch` statement is checking the value of the `Digits` input parameter sent from the
		user's phone keypad. Depending on the value of `Digits`, the code will execute the
		corresponding `case` block. If `Digits` is equal to '1', the code will execute the first
		`case` block, which will generate a TwiML response with a message thanking the user for
		selecting 1 and logging the message. If `Digits` is equal to '2', the code will execute the
		second `case` block, which will generate a TwiML response with a message thanking the user
		for selecting 2 and logging the message. If `Digits` is not equal to either '1' or '2', the
		code will execute the `default` block, which will generate a TwiML response with a message
		apologizing for not understanding the input and logging the message. */
		switch ($request->input('Digits')) {
			case '1':
				$twiml->say('Thank you for selecting 1. We will contact you shortly.');
				Log::info('Thank you for selecting 1. We will contact you shortly.');
				break;
			case '2':
				$twiml->say('Thank you for selecting 2. We will contact you shortly.');
				Log::info('Thank you for selecting 2. We will contact you shortly.');
				break;
			default:
				$twiml->say('Sorry, I did not understand your input. Goodbye.');
				Log::info('Sorry, I did not understand your input. Goodbye.');
				break;
		}

		/* `->hangup();` is adding a TwiML command to the response to hang up the call. This
		command tells Twilio to end the call after the message has been played. */
		$twiml->hangup();

		return response($twiml)->header('Content-Type', 'text/xml');
	}


	//ENDS
}
