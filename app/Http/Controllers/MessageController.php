<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;

class MessageController extends Controller
{

	/**
	 * It returns a view called `backend.messages.index`
	 *
	 * @return The view file located at resources/views/backend/messages/index.blade.php
	 */
	public function index()
	{
		return view('backend.messages.index');
	}

	/**
	 * It returns a view called `message` from the `backend.messages` folder
	 *
	 * @param user_number The user number of the user you want to send the message to.
	 *
	 * @return A view called message.blade.php
	 */
	public function message($my_number, $user_number = null)
	{
		/* Checking if the user number is not null. If it is not null, it calls the `message_as_seen`
		function. */
		if ($user_number != null) {
			message_as_seen($user_number, $my_number);
		}
		return view('backend.messages.message', compact('user_number', 'my_number'));
	}

	/**
	 * It receives an incoming message, saves it to the database, and returns a response to the sender
	 *
	 * @param Request request The incoming request from Twilio.
	 */
	public function processIncomingMessage(Request $request)
	{
		try {
			$sender = $request->input('From');
			$recipient = $request->input('To');
			$content = $request->input('Body');
			$sentAt = now();

			$user_id = find_user_id_by_number($recipient);

			// Save message to the database
			DB::table('messages')->insert([
				'sender' => $sender,
				'recipient' => $recipient,
				'content' => $content,
				'user_number' => $sender,
				'my_number' => $recipient,
				'sent_at' => $sentAt,
				'seen' => 0,
				'user_id' => $user_id,
				'created_at' => now(),
				'updated_at' => now(),
			]);
		} catch (\Throwable $th) {
			throw $th;
		}
	}

	/**
	 * It returns a JSON response of all the messages in the database, ordered by the time they were
	 * sent
	 *
	 * @return A JSON object containing all the messages in the database.
	 */
	public function show($my_number, $user_number)
	{

		/* Checking if the user number is not null. If it is not null, it calls the `message_as_seen`
		function. */
		if ($user_number != null) {
			message_as_seen($user_number, $my_number);
		}

		$messages = conversations($user_number, $my_number);
		return response()->json($messages);
	}

	/* A function that sends a message to a recipient. */
	public function send(Request $request, $user_number, $my_number)
	{
		// Get the message content and recipient phone number from the request
		$content = $request->content;
		$sentAt = now();

		$accountSid = user_provider_info(auth()->id())->account_sid;
		$authToken = user_provider_info(auth()->id())->auth_token;
		$phone_number = $my_number;

		// Send the message using the Twilio API
		$twilio = new Client($accountSid, $authToken);

		/* Sending a message to the recipient using the Twilio API. */
		$message = $twilio->messages->create(
			$user_number,
			[
				'from' => $phone_number,
				'body' => $content,
			]
		);

		// Return a response indicating success or failure
		if ($message->sid) {
			// Save message to the database
			DB::table('messages')->insert([
				'sender' => $phone_number,
				'recipient' => $user_number,
				'content' => $content,
				'user_number' => $user_number,
				'my_number' => $my_number,
				'sent_at' => $sentAt,
				'seen' => 1,
				'user_id' => auth()->id(),
				'created_at' => now(),
				'updated_at' => now(),
			]);

			return response()->json(['status' => 'success']);
		} else {
			return response()->json(['status' => 'error']);
		}
	}

	public function compose_new_message(Request $request, $my_number)
	{
		// Get the message content and recipient phone number from the request
		$content = $request->content;
		$to = $request->phone;
		$sentAt = now();

		$accountSid = user_provider_info(auth()->id())->account_sid;
		$authToken = user_provider_info(auth()->id())->auth_token;
		$phone_number = $my_number;

		// Send the message using the Twilio API
		$twilio = new Client($accountSid, $authToken);

		/* Sending a message to the recipient using the Twilio API. */
		$message = $twilio->messages->create(
			$to,
			[
				'from' => $phone_number,
				'body' => $content,
			]
		);

		// Return a response indicating success or failure
		if ($message->sid) {
			// Save message to the database
			DB::table('messages')->insert([
				'sender' => $phone_number,
				'recipient' => $to,
				'content' => $content,
				'user_number' => $to,
				'my_number' => $my_number,
				'sent_at' => $sentAt,
				'seen' => 1,
				'user_id' => auth()->id(),
				'created_at' => now(),
				'updated_at' => now(),
			]);

			smilify('success', 'Message sent successfully.');
			return back();
		} else {
			smilify('error', 'Something went wrong.');
			return back();
		}
	}

	/**
	 * This PHP function fetches unseen messages for a specific user and returns them as a JSON
	 * response.
	 *
	 * @param Request request  is an instance of the Illuminate\Http\Request class, which
	 * represents an HTTP request. It contains information about the request such as the HTTP method,
	 * headers, and input data. In this function, it is used to retrieve any input data that may have
	 * been sent with the request.
	 * @param my_number The parameter "my_number" is likely a column in the "messages" table that
	 * stores the phone number of the user who sent or received the message. The function is using this
	 * parameter to filter the messages and retrieve only those messages that belong to the user with
	 * the specified phone number.
	 *
	 * @return a JSON response containing all the messages that match the given criteria (where
	 * my_number is equal to , user_id is equal to the authenticated user's ID, and seen is
	 * equal to 0), sorted by sent_at in descending order.
	 */
	public function messages_ajax_fetch($my_number)
	{
		$messages = new_message_found($my_number);
		return response()->json($messages);
	}
	//ENDS
}
