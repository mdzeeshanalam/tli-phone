<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SmtpController extends Controller
{
	public function index()
	{
		return view('backend.smtp.index');
	}

	public function store(Request $request)
	{
		if (demo()) {
			smilify('warning', 'This feature is disabled in demo mode');

			return back();
		}

		overWriteEnvFile('MAIL_MAILER', $request->driver);
		overWriteEnvFile('MAIL_HOST', $request->host);
		overWriteEnvFile('MAIL_PORT', $request->port);
		overWriteEnvFile('MAIL_USERNAME', $request->username);
		overWriteEnvFile('MAIL_PASSWORD', $request->password);
		overWriteEnvFile('MAIL_ENCRYPTION', $request->encryption);
		overWriteEnvFile('MAIL_FROM_ADDRESS', $request->from);
		overWriteEnvFile('MAIL_FROM_NAME', $request->from_name);
		smilify('success', 'Mail settings updated successfully');

		return back();
	}
	//ENDS HERE
}
