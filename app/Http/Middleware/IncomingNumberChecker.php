<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IncomingNumberChecker
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
	 * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
	 */
	public function handle(Request $request, Closure $next)
	{

		$check_number = getProvider(auth()->id(), $request->my_number);

		if ($check_number) {
			return $next($request);
		} else {
			return abort(404);
		}

	}
}
