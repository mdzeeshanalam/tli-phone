<?php

namespace App\Http\Middleware;

use App\Models\Agent;
use Auth;
use Closure;
use Illuminate\Http\Request;

class DocumentsVerify
{
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
	 * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
	 */
	public function handle(Request $request, Closure $next)
	{

		if (tliphone_config('kyc') == "YES") {
			if (is_admin(Auth::id()) == true) {
				return $next($request);
			}

			if (is_agent(Auth::id())) {
				$owner_id = Agent::where('user_id', Auth::id())->first()->assined_for_customer_id;

				if (kyc_verified($owner_id) == true) {
					return $next($request);
				} else {
					smilify('warning', 'Account KYC is not verified. Please inform the owner.'); // smilify info
					return redirect()->route('backend');
				}
			}

			if (kyc_verified(Auth::id()) == true) {
				return $next($request);
			} else {
				smilify('warning', 'You have to verify your KYC document to access this feature.'); // smilify info
				return redirect()->route('dashboard.kyc.index');
			}
		} else {
			return $next($request);
		}

	}

	// ENDS
}
