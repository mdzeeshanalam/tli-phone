﻿"use strict"

$(function () {
  var speakerDevices = document.getElementById('speaker-devices'); 
  var ringtoneDevices = document.getElementById('ringtone-devices');
  var outputVolumeBar = document.getElementById('output-volume');
  var inputVolumeBar = document.getElementById('input-volume');
  var volumeIndicators = document.getElementById('volume-indicators');
  var check_phone_number = document.getElementById('phone-number');
  var capability_token = document.getElementById('capability_token').value; 

  var dialer_call_duration = document.getElementById('dialer_call_duration').value;
  var dialer_country_code_exists_in_package = document.getElementById('dialer_country_code_exists_in_package').value;

  log('Connecting........');
  $.getJSON(`${capability_token}`)
  //Paste URL HERE
    .done(function (data) {
      log('Verifying identification......');

      // Setup Twilio.Device
      Twilio.Device.setup(data.token);

      log('identity: ' + data.identity);

      Twilio.Device.ready(function (device) {
        log('Device is ready to make calls.');
        document.getElementById('call-controls').style.display = 'block';
      });

      Twilio.Device.error(function (error) {
        log('Device Error: ' + error.message);
      });

      Twilio.Device.connect(function (conn) {
        log('Successfully established call!');
        document.getElementById('button-call').style.display = 'none';
        document.getElementById('button-hangup').style.display = 'inline';
        volumeIndicators.style.display = 'block';
        bindVolumeIndicators(conn);
      });

      Twilio.Device.disconnect(function (conn) {
        log('Call ended.');
        document.getElementById('button-call').style.display = 'inline';
        document.getElementById('button-hangup').style.display = 'none';
        document.getElementById('call-name').innerHTML = 'Call Ended';
        volumeIndicators.style.display = 'none';

        /**
         * Call end Time
         */

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

    // check if session storage has dialer_session_uuid
    if (sessionStorage.getItem('dialer_session_uuid') != null) {

      // ajax request to store the contact lead
      $.ajax({
          url: dialer_call_duration,
          type: "POST",
          data: {
              dialer_session_uuid: sessionStorage.getItem('dialer_session_uuid'),
              end_at: 'now',
          },
          success: function (data) {
            // get current time
            var current_time = new Date();
            var time = current_time.getHours() + ":" + current_time.getMinutes() + ":" + current_time.getSeconds();
            // log the call start time
              toastr.info('Call ended at ' + time);
              log('Call ended at ' + time);
              sessionStorage.removeItem('dialer_session_uuid');
          },
          error: function (error) {
              toastr.remove();
              toastr.error('Something went wrong!');
              log('Something went wrong!');
          }
      });

      
    }else{
      toastr.warning('No call to hangup');
      log('No call to hangup');
      return;
    }

    /**
    * Call end Time::ENDS
    */


      });

      setClientNameUI(data.identity);

      Twilio.Device.audio.on('deviceChange', updateAllDevices);

      // Show audio selection UI if it is supported by the browser.
      if (Twilio.Device.audio.isSelectionSupported) {
        document.getElementById('output-selection').style.display = 'block';
      }
    })
    .fail(function () {
      // log('Could not get a token from server!');
      log('Verfication failed!');
    });

  // Bind button to make call
  document.getElementById('button-call').onclick = function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Check the phone number is not empty
     */
    if (check_phone_number.value === '') {
      toastr.remove();
      toastr.error('Please enter a valid phone number');
      log('Please enter a valid phone number');
      return;
    }

    /**
     * EXTRA CHARGE
     */
    $.ajax({
      url: dialer_country_code_exists_in_package,
      type: "POST",
      data: {
        phone: document.getElementById('phone-number').value,
      },
      success: function(data){

        if (data.status === 'error') { // if the call was successful
            toastr.error(data.message);
            Swal.fire(data.message);
            log('warning: ' + data.message);

            // if unsuppoted number hang up the call
            if (data.message === 'Unsupported number.') {
              Twilio.Device.disconnectAll();
              toastr.info('Hanging up...');
              log(document.getElementById('phone-number').value + ' is not a supported number');
              log('Hanging up...');
            }

        }// if the call was successful
      }
    });

    /**
     * EXTRA CHARGE::ENDS
     */

    // get the phone number to connect the call to
    document.getElementById('call-name').innerHTML = 'Calling';
    document.getElementById('call-number').innerHTML = document.getElementById('phone-number').value;
    var params = {
      To: document.getElementById('phone-number').value
    };

    // check if session storage has dialer_session_uuid
    if (sessionStorage.getItem('dialer_session_uuid') === null) { // if not, create a new one
      sessionStorage.setItem('dialer_session_uuid', uuidv4()); // set the session uuid

      // ajax request to store the contact lead
      $.ajax({
          url: dialer_call_duration,
          type: "POST",
          data: {
              dialer_session_uuid: sessionStorage.getItem('dialer_session_uuid'),
              phone: document.getElementById('phone-number').value,
              start_at: 'now',
          },
          success: function (data) { 
            // get current time
            
            if (data.status === 'success') { // if the call was successful
              var current_time = new Date();
              var time = current_time.getHours() + ":" + current_time.getMinutes() + ":" + current_time.getSeconds();
              // log the call start time
              toastr.info('Calling ' + params.To);
              toastr.info('Call started at ' + time);
              log('Calling ' + params.To);
              log('Call started at ' + time);
              Twilio.Device.connect(params);

            }else{ // if the call was not successful
              sessionStorage.removeItem('dialer_session_uuid', uuidv4());
              toastr.error(data.message);
              log('Error: ' + data.message);
              return false;
            } // end of if data.status
          }, // end of success
          error: function (error) { // if the call was not successful
              sessionStorage.removeItem('dialer_session_uuid', uuidv4());
              toastr.error(error.message);
              log('Something went wrong!');
              return false;
          } // end of error
      }); // end of ajax request

    }else{ // if session storage has dialer_session_uuid
      toastr.warning('You are already in a call');
      log('warning: You are already in a call!');
      return false;
    } // end of if session storage has dialer_session_uuid

  };

  // Bind button to hangup call
  document.getElementById('button-hangup').onclick = function () {
    document.getElementById('call-name').innerHTML = 'Call Ended';
    document.getElementById('call-number').innerHTML = document.getElementById('phone-number').value;

    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

    // check if session storage has dialer_session_uuid
    if (sessionStorage.getItem('dialer_session_uuid') != null) {

      // ajax request to store the contact lead
      $.ajax({
          url: dialer_call_duration,
          type: "POST",
          data: {
              dialer_session_uuid: sessionStorage.getItem('dialer_session_uuid'),
              end_at: 'now',
          },
          success: function (data) {
            console.log(data);
            // get current time
            var current_time = new Date();
            var time = current_time.getHours() + ":" + current_time.getMinutes() + ":" + current_time.getSeconds();
            // log the call start time
              toastr.info('Call ended at ' + time);
              log('Call ended at ' + time);
              sessionStorage.removeItem('dialer_session_uuid');
          },
          error: function (error) {
              toastr.remove();
              toastr.error('Something went wrong!');
              log('Something went wrong!');
          }
      });

      
    }else{
      toastr.warning('No call to hangup');
      log('warning: No call to hangup!');
      return;
    }

    toastr.info('Hanging up...');
    log('Hanging up...');
    Twilio.Device.disconnectAll();
  };

  document.getElementById('get-devices').onclick = function() {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(updateAllDevices);
  };

  speakerDevices.addEventListener('change', function() {
    var selectedDevices = [].slice.call(speakerDevices.children)
      .filter(function(node) { return node.selected; })
      .map(function(node) { return node.getAttribute('data-id'); });
    
    Twilio.Device.audio.speakerDevices.set(selectedDevices);
  });

  ringtoneDevices.addEventListener('change', function() {
    var selectedDevices = [].slice.call(ringtoneDevices.children)
      .filter(function(node) { return node.selected; })
      .map(function(node) { return node.getAttribute('data-id'); });
    
    Twilio.Device.audio.ringtoneDevices.set(selectedDevices);
  });

  function bindVolumeIndicators(connection) {
    connection.volume(function(inputVolume, outputVolume) {
      var inputColor = 'red';
      if (inputVolume < .50) {
        inputColor = 'green';
      } else if (inputVolume < .75) {
        inputColor = 'yellow';
      }

      inputVolumeBar.style.width = Math.floor(inputVolume * 300) + 'px';
      inputVolumeBar.style.background = inputColor;

      var outputColor = 'red';
      if (outputVolume < .50) {
        outputColor = 'green';
      } else if (outputVolume < .75) {
        outputColor = 'yellow';
      }

      outputVolumeBar.style.width = Math.floor(outputVolume * 300) + 'px';
      outputVolumeBar.style.background = outputColor;
    });
  }

  function updateAllDevices() {
    updateDevices(speakerDevices, Twilio.Device.audio.speakerDevices.get());
    updateDevices(ringtoneDevices, Twilio.Device.audio.ringtoneDevices.get());
  }
});

// Update the available ringtone and speaker devices
function updateDevices(selectEl, selectedDevices) {
  selectEl.innerHTML = '';
  Twilio.Device.audio.availableOutputDevices.forEach(function(device, id) {
    var isActive = (selectedDevices.size === 0 && id === 'default');
    selectedDevices.forEach(function(device) {
      if (device.deviceId === id) { isActive = true; }
    });

    var option = document.createElement('option');
    option.label = device.label;
    option.setAttribute('data-id', id);
    if (isActive) {
      option.setAttribute('selected', 'selected');
    }
    selectEl.appendChild(option);
  });
}

// Activity log
function log(message) {
  var logDiv = document.getElementById('log');
  logDiv.innerHTML += '<p>⇢ ' + message + '</p>';
  logDiv.scrollTop = logDiv.scrollHeight;
}

// Set the client name in the UI
function setClientNameUI(clientName) {
  var div = document.getElementById('client-name');
}


function MakeVoiceCall(sl_id = null, st_id = null, phone_number, phone, campaign, store_data = false, make_call, status = 'd') {

  var dialer_call_duration = document.getElementById('dialer_call_duration').value;
  var dialer_country_code_exists_in_package = document.getElementById('dialer_country_code_exists_in_package').value;

  if (make_call != null) { //  if the call was not successful
    
    document.getElementById('call-name').innerHTML = 'Calling';
    document.getElementById('phone-number').value = phone_number;
    document.getElementById('call-number').innerHTML = phone_number;
    var params = {
      To: document.getElementById('phone-number').value
    };

    // check if session storage has dialer_session_uuid
    if (sessionStorage.getItem('dialer_session_uuid') === null) { // if not, create a new one
      if (make_call == true) {
        sessionStorage.setItem('dialer_session_uuid', uuidv4()); // set the session uuid
      }

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      /**
     * EXTRA CHARGE
     */
      $.ajax({
        url: dialer_country_code_exists_in_package,
        type: "POST",
        data: {
          phone: document.getElementById('phone-number').value,
        },
        success: function(data){

          if (data.status === 'error') { // if the call was successful
              toastr.error(data.message);
              Swal.fire(data.message);
              log('warning: ' + data.message);
          }// if the call was successful
        }
      });
    /**
     * EXTRA CHARGE::ENDS
     */

      // ajax request to store the contact lead
      $.ajax({
          url: dialer_call_duration,
          type: "POST",
          data: {
              dialer_session_uuid: sessionStorage.getItem('dialer_session_uuid'),
              phone: phone_number,
              start_at: 'now',
          },
          success: function (data) { 
            // get current time
            
            if (data.status === 'success') { // if the call was successful

              if (make_call == true) {
                var current_time = new Date();
                var time = current_time.getHours() + ":" + current_time.getMinutes() + ":" + current_time.getSeconds();
                // log the call start time
                toastr.info('Calling ' + params.To);
                toastr.info('Call started at ' + time);
                log('Calling ' + params.To);
                log('Call started at ' + time);
                Twilio.Device.connect(params);
              }

              if (store_data) { // if store_data is true, then store the data in the database
                var url = $('#dashboard_campaign_voice_lead').val();

                  $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                  
                  // ajax request to store the contact lead
                  $.ajax({
                      url: url,
                      type: "POST",
                      data: { // store the data in the database
                          phone: phone,
                          number: phone_number,
                          campaign_id: campaign,
                          status: status
                      },
                      success: function (data) { // if the call was successful
                          toastr.info(data.success);
                          log(data.success);
                          // change the selected button class
                          if (make_call == true) {
                            $('#' + sl_id).removeClass('btn-secondary');
                            $('#' + sl_id).addClass('btn-success');
                          }else{
                            $('#' + st_id + status).removeClass('btn-secondary');
                            $('#' + st_id + status).addClass('btn-info');
                          }
                      },
                      error: function (data) {
                          toastr.error('Something went wrong!');
                          log('Something went wrong!');
                      }
                  });
              } // end of if store_data

            }else{ // if the call was not successful
              sessionStorage.removeItem('dialer_session_uuid', uuidv4());
              toastr.error(data.message);
              return false;
            } // end of if data.status
          }, // end of success
          error: function (error) { // if the call was not successful
              sessionStorage.removeItem('dialer_session_uuid', uuidv4());
              toastr.error('Something went wrong!');
              log('Something went wrong!');
              return false;
          } // end of error
      }); // end of ajax request

    }else{ // if session storage has dialer_session_uuid
      toastr.warning('You are already in a call');
      log('warning: You are already in a call');
      return false;
    } // end of if session storage has dialer_session_uuid
    
  }else{ // end of if make_call
    toastr.warning('Please make a call first!');
    log('warning: Please make a call first!');
    return false;
  } // end of if make_call

}

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}
