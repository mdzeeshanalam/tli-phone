<!-- BASE CSS -->
<link href="{{ asset('subscription/css/bootstrap.css') }}" rel="stylesheet">
<link href="{{ asset('subscription/css/menu.css') }}" rel="stylesheet">
<link href="{{ asset('subscription/css/style.css') }}" rel="stylesheet">
<link href="{{ asset('subscription/css/vendors.css') }}" rel="stylesheet">

@notifyCss

<!-- YOUR CUSTOM CSS -->

@yield('css')

<!-- MODERNIZR MENU -->
<script src="{{ asset('subscription/js/modernizr.js') }}"></script>

