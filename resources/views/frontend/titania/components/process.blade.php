<div id="about" class="section is-app-grey is-medium">
        <div class="container">
            <!-- Title -->
            <div class="section-title-wrapper has-text-centered">
                <div class="bg-number">2</div>
                <h2 class="section-title-landing editable is-modified" data-cid="17" tabindex="1">{{ saasContent(17) ?? 'How it Works' }}</h2>
                <h4 class="editable is-modified" data-cid="18" tabindex="1">{{ saasContent(18) ?? 'Our business model is very simple' }}</h4>
            </div>

            <!-- Process steps -->
            <div class="content-wrapper">
                <div class="columns is-vcentered">
                    <!-- Process step -->
                    <div class="column is-4">
                        <div class="process-block has-line">
                            <div class="process-icon is-icon-reveal">
                                <div class="icon-wrapper">
                                    <i class="im im-icon-Arrow-Over"></i>
                                    <div class="process-number">1</div>
                                </div>
                            </div>
                            <div class="process-info">
                                <div class="step-number">1</div>
                                <div class="details">
                                    <div class="motto editable is-modified" data-cid="19" tabindex="1">{{ saasContent(19) ?? 'Think' }}</div>
                                    <p class="description editable is-modified" data-cid="20" tabindex="1">
                                        {{ saasContent(20) ?? 'Lorem ipsum dolor sit amet, eam ex probo tation tractatos. Ut
                                        vel hinc solet tincidunt, nec et iisque placerat pertinax. Ei
                                        minim probatus mea.' }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Process step -->
                    <div class="column is-4">
                        <div class="process-block has-line">
                            <div class="process-icon is-icon-reveal">
                                <div class="icon-wrapper">
                                    <i class="im im-icon-Arrow-Around"></i>
                                    <div class="process-number">2</div>
                                </div>
                            </div>
                            <div class="process-info">
                                <div class="step-number">2</div>
                                <div class="details">
                                    <div class="motto editable is-modified" data-cid="21" tabindex="1">{{ saasContent(21) ?? 'Iterate' }}</div>
                                    <p class="description editable is-modified" data-cid="22" tabindex="1">
                                        {{ saasContent(22) ?? 'Lorem ipsum dolor sit amet, eam ex probo tation tractatos. Ut
                                        vel hinc solet tincidunt, nec et iisque placerat pertinax. Ei
                                        minim probatus mea.' }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Process step -->
                    <div class="column is-4">
                        <div class="process-block">
                            <div class="process-icon is-icon-reveal">
                                <div class="icon-wrapper">
                                    <i class="im im-icon-Arrow-Refresh"></i>
                                    <div class="process-number">3</div>
                                </div>
                            </div>
                            <div class="process-info">
                                <div class="step-number">3</div>
                                <div class="details">
                                    <div class="motto editable is-modified" data-cid="23" tabindex="1">{{ saasContent(23) ?? 'Create' }}</div>
                                    <p class="description editable is-modified" data-cid="24" tabindex="1">
                                        {{ saasContent(24) ?? 'Lorem ipsum dolor sit amet, eam ex probo tation tractatos. Ut
                                        vel hinc solet tincidunt, nec et iisque placerat pertinax. Ei
                                        minim probatus mea.' }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>