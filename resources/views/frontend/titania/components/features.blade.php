<div id="product" class="section is-pricing is-medium is-skewed-sm">
        <div class="container is-reverse-skewed-sm">
            <!-- Title -->
            <div class="section-title-wrapper has-text-centered">
                <div class="bg-number">1</div>
                <h2 class="section-title-landing editable is-modified" data-cid="3" tabindex="1">{{ saasContent(3) ?? 'Sales just got Simple' }}</h2>
                <h4 class="editable is-modified" data-cid="4" tabindex="1">{{ saasContent(4) ?? 'Learn to sell better and faster' }}</h4>
            </div>

            <div class="content-wrapper">

                <!-- Icon boxes -->
                <div class="columns is-vcentered is-multiline has-text-centered">
                    <div class="column"></div>
                    <div class="column is-3">
                        <!-- Icon box -->
                        <div class="icon-box primary">
                            <div class="box-icon primary">
                                <i class="im im-icon-Credit-Card2"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="5" tabindex="1">{{ saasContent(5) ?? 'One time payment' }}</div>
                            <div class="box-text editable is-modified" data-cid="6" tabindex="1">
                                {{ saasContent(6) ?? 'Lorem ipsum dolor sit amet, usu no ancillae verterem partiendo,
                                mea ea.' }}
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="icon-box primary mt-40">
                            <div class="box-icon primary">
                                <i class="im im-icon-Time-Backup"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="7" tabindex="1">{{ saasContent(7) ?? 'Sales just got Simple' }}Backup automation</div>
                            <div class="box-text editable is-modified" data-cid="8" tabindex="1">
                                {{ saasContent(8) ?? 'Sales just got Simple' }}
                                mea ea.
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <!-- Icon box -->
                        <div class="icon-box primary">
                            <div class="box-icon primary">
                                <i class="im im-icon-Download-fromCloud"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="9" tabindex="1">{{ saasContent(9) ?? 'Instant downloads' }}</div>
                            <div class="box-text editable is-modified" data-cid="10" tabindex="1">
                                {{ saasContent(10) ?? 'Lorem ipsum dolor sit amet, usu no ancillae verterem partiendo,
                                mea ea.' }}
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="icon-box primary mt-40">
                            <div class="box-icon primary">
                                <i class="im im-icon-Speach-Bubble2"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="11" tabindex="1">{{ saasContent(11) ?? 'Direct messaging' }}</div>
                            <div class="box-text editable is-modified" data-cid="12" tabindex="1">
                                {{ saasContent(12) ?? 'Lorem ipsum dolor sit amet, usu no ancillae verterem partiendo,
                                mea ea.' }}
                            </div>
                        </div>
                    </div>
                    <div class="column is-3">
                        <!-- Icon box -->
                        <div class="icon-box primary">
                            <div class="box-icon primary">
                                <i class="im im-icon-Dropbox"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="13" tabindex="1">{{ saasContent(13) ?? 'Free storage' }}</div>
                            <div class="box-text editable is-modified" data-cid="14" tabindex="1">
                                {{ saasContent(14) ?? 'Lorem ipsum dolor sit amet, usu no ancillae verterem partiendo,
                                mea ea.' }}
                            </div>
                        </div>
                        <!-- Icon box -->
                        <div class="icon-box primary mt-40">
                            <div class="box-icon primary">
                                <i class="im im-icon-Stopwatch"></i>
                            </div>
                            <div class="box-title editable is-modified" data-cid="15" tabindex="1">{{ saasContent(15) ?? 'Time tracking' }}</div>
                            <div class="box-text editable is-modified" data-cid="16" tabindex="1">
                                {{ saasContent(16) ?? 'Lorem ipsum dolor sit amet, usu no ancillae verterem partiendo,
                                mea ea.' }}
                            </div>
                        </div>
                    </div>
                    <div class="column"></div>
                </div>
            </div>
        </div>
    </div>