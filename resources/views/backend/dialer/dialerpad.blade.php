<!DOCTYPE html>
<html>

<head>
    <title>{{ translate('Inbound Calling') }}</title>
    @includeWhen(true, 'backend.layouts.components.meta')
    @includeWhen(true, 'backend.layouts.components.css')
</head>

<body>

    <div class="container">
        <h6 class="lead text-center p-4">{{ translate('Identity') }}: {{ get_user_identity(auth()->id()) }}</h6>

        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-12 text-center" id="dialer-message">
                <h3 class="lead">{{ translate('Verifying device') }}</h3>
                {{ lordicon('ritcuqlt', 'xjovhxra', 'loop', '000000', '7789fb', 250) }}
            </div>
        </div>

        <div class="row d-none" id="dialerpad">
            <div class="col-md-4 d-flex justify-content-center align-items-center">  

                <div class="card shadow d-none" id="dialer_card">    
                    <div class="card-inner">        
                        <h5 class="card-title lead text-center">{{ translate('Incoming call by') }} : </h5>
                        <h4 class="text-center lead p-4" id="display-number"></h4>
                        <p class="text-center lead" id="dialer-timer"></p>
                        <div class="m-auto text-center">
                        <a href="javascript:;" class="btn btn-icon btn-lg btn-success call-btn call-accept" id="button-call">
                            <em class="icon ni ni-call"></em>
                        </a>
                        <a href="javascript:;" class="btn btn-icon btn-lg btn-danger call-btn call-reject" id="button-hangup">
                            <em class="icon ni ni-cross-c"></em>
                        </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-8">


                <div class="card-inner card-inner-lg">
                
                    <div class="nk-block">

                        <div class="justify-content-center align-items-center text-center d-none" id="no_caller_data_found">
                            {{ lordicon('ritcuqlt', 'eszyyflr', 'morph', '000000', '7789fb', 250) }}
                            <h5 class="lead">{{ translate('No Caller data found.') }}</h5>
                        </div>

                        <div class="nk-data data-list d-none" id="caller_information">
                            <div class="data-head">
                                <h6 class="overline-title">{{ translate('Caller Information') }}</h6>
                            </div>
                            <div class="data-item">
                                <div class="data-col">
                                    <span class="data-label">{{ translate('Full Name') }}</span>
                                    <span class="data-value" id="full_name"></span>
                                </div>
                            </div>
                            <div class="data-item">
                                <div class="data-col">
                                    <span class="data-label">{{ translate('Country') }}</span>
                                    <span class="data-value" id="contact_country"></span>
                                </div>
                            </div>
                            <div class="data-item">
                                <div class="data-col">
                                    <span class="data-label">{{ translate('Gender') }}</span>
                                    <span class="data-value" id="contact_gender"></span>
                                </div>
                            </div>
                            <div class="data-item">
                                <div class="data-col">
                                    <span class="data-label">{{ translate('Date Of Birth') }}</span>
                                    <span class="data-value" id="contact_dob"></span>
                                </div>
                            </div>
                            <div class="data-item">
                                <div class="data-col">
                                    <span class="data-label">{{ translate('Profession') }}</span>
                                    <span class="data-value" id="contact_profession"></span>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>


            </div>
        </div>

        @if (count(getCallHistories(get_user_identity_id(auth()->id()))) > 0)
        
        <div class="row">
            <div class="col-md-12">
                <div class="card card-preview">
                    <div class="card-inner">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <input type="text" class="form-control" id="search_input" placeholder="{{ translate('Search here') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-preview overflow-auto" style="height: 90vh;">
                    <div class="card-inner">
                        <table class="datatable-init nk-tb-list nk-tb-ulist search-table" data-auto-responsive="true">
                            <thead>
                                <tr class="nk-tb-item nk-tb-head">
                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">{{ translate('SL.') }}</span></th>
                                    <th class="nk-tb-col tb-col-mb"><span class="sub-text">{{ translate('CALLER PHONE') }}</span></th>
                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">{{ translate('INCOMING') }}</span></th>
                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">{{ translate('HANGUP') }}</span></th>
                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">{{ translate('DURATION') }}</span></th>
                                    <th class="nk-tb-col tb-col-md"><span class="sub-text">{{ translate('STATUS') }}</span></th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse (getCallHistories(get_user_identity_id(auth()->id())) as $history)
                                <tr class="nk-tb-item">
                                    <td class="nk-tb-col">
                                        <div class="user-card">
                                            <div class="user-avatar bg-dim-primary d-none d-sm-flex">
                                                <span>{{ $loop->iteration }}</span>
                                            </div>
                                        </div>
                                    </td>
                                
                                    <td class="nk-tb-col tb-col-mb">
                                        <span class="tb-amount">{{ $history->caller_number ?? '--' }}</span>
                                    </td>
                                
                                    <td class="nk-tb-col tb-col-mb">
                                        <span class="tb-amount">{{ $history->pick_up_time ?? '--' }}</span>
                                    </td>
                                
                                    <td class="nk-tb-col tb-col-mb">
                                        <span class="tb-amount">{{ $history->hang_up_time ?? '--' }}</span>
                                    </td>
                                
                                    <td class="nk-tb-col tb-col-mb">
                                        <span class="tb-amount">
                                            @if ($history->pick_up_time && $history->hang_up_time)
                                                {{ calculateCallDuration($history->pick_up_time, $history->hang_up_time) }} 
                                            @else
                                            --
                                            @endif
                                        </span>
                                    </td>
                                
                                    <td class="nk-tb-col tb-col-md">
                                        <span class="tb-status text-bold">
                                            {{ Str::upper($history->status) }}
                                        </span>
                                    </td>
                                    
                                </tr><!-- .nk-tb-item  -->
                            @empty
                                    
                            @endforelse
                        
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        @endif

    </div>

    {{-- HIDDEN::START --}}
    <div id="info" class="d-none">
        <p class="instructions">Twilio Client</p>
        <div id="client-name"></div>
        <div id="output-selection">
            <label>Ringtone Devices</label>
            <select id="ringtone-devices" multiple></select>
            <label>Speaker Devices</label>
            <select id="speaker-devices" multiple></select><br />
            <a id="get-devices">Seeing unknown devices?</a>
        </div>
    </div>
    <div id="call-controls" class="d-none">
        <p class="instructions">Make a Call:</p>
        <div id="volume-indicators">
            <label>Mic Volume</label>
            <div id="input-volume"></div><br /><br />
            <label>Speaker Volume</label>
            <div id="output-volume"></div>
        </div>
    </div>
    <div id="log" class="d-none"></div>
    <div id="client-name" class="d-none"></div>
    {{-- HIDDEN::ENDS --}}


    <input type="hidden" id="capability_token" value="{{ route('dialer.token') }}">
    <input type="hidden" id="create_call_history_url" value="{{ route('create.call.hostory') }}">
    <input type="hidden" id="my_number" value="{{ $my_number }}">
    <input type="hidden" id="find_contact_url" value="{{ route('dashboard.contact.find') }}">

    <script type="text/javascript" src="{{ asset('dialerpad/js/twilio.js') }}"></script>
    <script src="{{ asset('dialerpad/js/jquery.min.js') }}"></script>
    <script src="{{ asset('dialerpad/js/quickstart.js') }}"></script>
</body>

</html>
