<div class="nk-sidebar nk-sidebar-mobile">
    <div class="nk-sidebar-inner" data-simplebar>
        <ul class="nk-menu nk-menu-md font-size-16">
            <li class="nk-menu-heading">
                <h5 class="overline-title text-primary-alt">{{ Str::replace('_', ' ', search_side_menu(Route::CurrentRouteName())) }}</h5>
            </li>
            
            @foreach (extended_menu() as $menuKey => $menu)
                @if ($menuKey == search_side_menu(Route::CurrentRouteName()))
                    @can($menu['permission'])
                        <li class="nk-menu-item @if(isset($menu['route_name'])) {{ request()->routeIs($menu['route_name']) ? 'has-sub' : '' }} @endif">
                            <a href="{{ isset($menu['route_name']) ? route($menu['route_name'], $menu['params']) : 'javascript:;' }}" 
                                class="nk-menu-link">
                                <span class="nk-menu-icon">
                                    <em class="icon ni {{ $menu['icon'] }}"></em>
                                </span>
                                <span class="nk-menu-text">{{ $menu['title'] }}</span>
                            </a>
                            @if (isset($menu['sub_menu']))
                                @foreach ($menu['sub_menu'] as $subMenuKey => $subMenu)
                                    @can($subMenu['permission'])
                                        <li class="nk-menu-item">
                                            <a href="{{ isset($subMenu['route_name']) ? route($subMenu['route_name'], $subMenu['params']) : 'javascript:;' }}" class="nk-menu-link">
                                                <span class="nk-menu-icon">
                                                    <em class="icon ni {{ $subMenu['icon'] }}"></em>
                                                </span>
                                                <span class="nk-menu-text">
                                                    {{ $subMenu['title'] }}
                                                </span>
                                            </a>
                                        </li>
                                    @endcan
                                @endforeach
                            @endif
                        </li>
                    @endcan
                @endif
            @endforeach

        </ul>
    </div>
</div>
