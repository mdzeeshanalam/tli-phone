@extends('backend.layouts.master')

@section('title')
    {{ translate('Creating New IVR') }}
@endsection

@section('css')
    
@endsection
    
@section('content')

    <div class="nk-block nk-block-lg">
    <div class="card card-preview">
        <div class="card-inner">

            <form action="{{ route('dashboard.ivr.store') }}" 
                  class="gy-3 form-validate is-alter" 
                  method="POST"
                  enctype="multipart/form-data">

                @csrf

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label" for="site-name">{{ translate('Ivr Name') }} *</label>
                            <span class="form-note">{{ translate('Specify the name of Ivr') }}.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <input type="text" 
                                       class="form-control" 
                                       id="site-name" 
                                       name="ivr_name" 
                                       value="{{ old('ivr_name') }}"
                                       placeholder="Ivr Name"
                                       required="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label">{{ translate('Package Duration Type') }} *</label>
                            <span class="form-note">{{ translate('Specify the URL if your main website is external') }}.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <select class="form-select" single="single"
                                    data-placeholder="Select Campaign" name="campaign_id" required="">
                                    @forelse (campaigns() as $campaign)
                                        <option value="{{ $campaign->id }}" {{ old('campaign_id') == $campaign->id ? 'selected' : null }}>{{ $campaign->name }}</option>
                                    @empty
                                        
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label" for="audio">{{ translate('Pre-recorded Audio Message') }}</label>
                            <span class="form-note">{{ translate('Specify the pre-recorded audio file') }}.</span>
                        </div>
                    </div>

                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <input type="file" 
                                        class="form-control" 
                                        id="audio" 
                                        name="audio_file" 
                                        value="{{ old('audio_file') }}">
                                <small>{{ translate('only .mp3 file is applicable') }}</small>
                            </div>
                        </div>
                    </div>
                </div>

                @for ($i = 0; $i < 10; $i++)

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label" for="key{{ $i }}_value">{{ translate('Keypad ') }}{{ $i }}</label>
                            <span class="form-note">{{ translate('Specify the Key ') }}{{ $i }}{{ translate(' value') }}.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">{{ $i }}</span>
                                    </div>
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Keypad {{ $i }} value" 
                                           id="key{{ $i }}_value"
                                           name="key{{ $i }}_value">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endfor

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label" for="key_star_value">{{ translate('Keypad *') }}</label>
                            <span class="form-note">{{ translate('Specify the Key * value') }}.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">*</span>
                                    </div>
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Keypad * value"
                                           id="key_star_value"
                                           name="key_star_value">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row g-3 align-center">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label class="form-label" for="key_hash_value">{{ translate('Keypad #') }}</label>
                            <span class="form-note">{{ translate('Specify the Key # value') }}.</span>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="form-group">
                            <div class="form-control-wrap">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">#</span>
                                    </div>
                                    <input type="text" 
                                           class="form-control" 
                                           placeholder="Keypad # value"
                                           id="key_hash_value"
                                           name="key_hash_value">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row g-3">
                    <div class="col-lg-7 offset-lg-5">
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-lg btn-secondary">{{ translate('Save') }}</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div><!-- .card-preview -->
</div>
<!-- END: Large Slide Over Toggle -->    

@endsection

@section('js')
    
@endsection