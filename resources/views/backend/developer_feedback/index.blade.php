@extends('backend.layouts.master')

@section('title')
{{ translate('Developer Feedback') }}
@endsection

@section('css')

@endsection

@section('content')

<div data-tf-widget="LGtyj9FO" data-tf-iframe-props="title=TLIPhone Bugs Report/Feedback" data-tf-medium="snippet"
    style="width:100%;height:400px;"></div>
<script src="//embed.typeform.com/next/embed.js"></script>

@endsection

@section('js')

@endsection
